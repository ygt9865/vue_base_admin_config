echo "-------打包脚本开始执行-------"
git status
git pull
git status
read -p "采用什么配置(sit|sit2|preview|staging|prod):" config
read -p "构建的是什么环境(preview|test|test2|prod|pretest):" commitinfo
read -p "提交到什么分支上:" branchName
read -p "采用"$config"配置，构建"$commitinfo"环境, 提交到"$branchName"分支, yes(y|Y) or no(n|N)" word

if [ "$word" = "" ] || [ "$word" = "no" ] || [ "$word" = "n" ] || [ "$word" = "N" ] ;then
  echo "--------End--------"
else
  if [ $config = "preview" ] || [ $config = "sit" ] || [ $config = "sit2" ] || [ $config = "prod" ] || [ $config = "staging" ] ;then
    yarn build:$config:all
    git status
    git add .
    git commit -m 'build(构建): 构建'$commitinfo'环境'
    if [ $commitinfo = "prod" ] || [ $commitinfo = "pretest" ] ;then
      echo $commitinfo"环境构建项目，不会自动提交到远程，需要手动提交"
    else
      git push origin $branchName
    fi
  else
    echo "输入的配置不存在，编译失败"
  fi
  echo "--------End--------"
  read -p "Enter ok and close"
fi
