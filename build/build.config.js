
// const glob = require('glob')
const { generateSplitChunksConfig } = require('./build.splitChunks.config')

// 获取构建时的项目名称
function getProjectName() {
  // const items = glob.sync('./src/project/*/main.js')
  // const indexItem = items[0]
  // const fileList = indexItem.split('/')
  // const fileName = fileList[fileList.length - 2]
  // const projectName = process.argv[5]
  // if (!projectName) throw new Error('请在yarn build或者npm run build 中指定指定需要构建的目录名称, 如yarn build xxx')
  // return projectName
  // return fileName
  return 'agents'
}

// 自动获取所有入口文件
// function getEntry() {
//   const items = glob.sync('./src/project/*/main.js')
//   // 获取需要启动的项目配置
//   // const serveProjectsConf = process.env.VUE_CLI_SERVE_PROJECTS || ''
//   // const activeProjects = serveProjectsConf.split(',').map(item => item.trim())

//   return items.reduce((entries, filepath) => {
//     const fileList = filepath.split('/')
//     const fileName = fileList[fileList.length - 2]

//     // if (serveProjectsConf === '' || activeProjects.includes(fileName)) {
//     //   entries[fileName] = {
//     //     entry: `src/project/${fileName}/main.js`,
//     //     // 模板来源
//     //     template: `public/index.html`,
//     //     // 在 dist/index.html 的输出
//     //     filename: `${fileName}.html`,
//     //     // 提取出来的通用 chunk 和 vendor chunk。
//     //     chunks: ['chunk-libs', 'chunk-elementUI', fileName]
//     //   }
//     // }
//     entries[fileName] = {
//       entry: `src/project/${fileName}/main.js`,
//       // 模板来源
//       template: `public/index.html`,
//       // 在 dist/index.html 的输出
//       filename: `${fileName}.html`,
//       // 提取出来的通用 chunk 和 vendor chunk。
//       chunks: ['chunk-libs', 'chunk-elementUI', fileName]
//     }
//     return entries
//   }, {})
// }

function getProductionConfig() {
  return {
    productionSourceMap: false,
    pages: {
      index: {
        // page的入口
        // entry: 'src/project/' + getProjectName() + '/main.js',
        entry: 'src/main.js',
        // 模板来源
        template: 'public/index.html',
        // 在 dist/index.html 的输出
        filename: 'index.html',
        title: getProjectName(),
        chunks: ['chunk-libs', 'chunk-elementUI', 'index']
      }
    },
    outputDir: `dist/${getProjectName()}`
  }
}

// console.log(getEntry())

module.exports = {
  development: {
    // pages: getEntry()
  },
  production: process.env.NODE_ENV === 'production' ? getProductionConfig() : {},
  chunksGroup: process.env.NODE_ENV === 'production' ? generateSplitChunksConfig(getProjectName()) : {}
}
