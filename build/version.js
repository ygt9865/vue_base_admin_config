const child_process = require('child_process')
console.log('NODE_ENV: ', process.env.NODE_ENV)
console.log('环境: ', process.env.VUE_APP_MODE)
console.log('请求域名: ', process.env.VUE_APP_BASE_API)
const envConfig = {
  env: process.env.NODE_ENV,
  appMode: process.env.VUE_APP_MODE,
  requestApi: process.env.VUE_APP_BASE_API
}

// git 最后一次提交的 Head
const commit = child_process.execSync('git show -s --format=%H').toString().trim()
const commitUserName = child_process.execSync('git show -s --format=%cn').toString().trim()
const commitUserMail = child_process.execSync('git show -s --format=%ce').toString().trim()
const commitDateObj = new Date(child_process.execSync(`git show -s --format=%cd`).toString())
const commitDate = `${commitDateObj.getFullYear()+'-'+(commitDateObj.getMonth()+1)+'-'+commitDateObj.getDate()+' '+commitDateObj.getHours()+':'+commitDateObj.getMinutes()}`
const buildUserName = child_process.execSync('git config user.name').toString().trim()
const buildUserMail = child_process.execSync('git config user.email').toString().trim()
const nowDate = new Date()
const buildDate = `${nowDate.getFullYear()+'-'+(nowDate.getMonth()+1)+'-'+nowDate.getDate()+' '+nowDate.getHours()+':'+nowDate.getMinutes()}`
// console.log({commit, commitUserName, commitUserMail, commitDate, buildUserName, buildUserMail, buildDate})
module.exports = {commit, commitUserName, commitUserMail, commitDate, buildUserName, buildUserMail, buildDate, envConfig}
