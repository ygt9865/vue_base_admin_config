const path = require('path')

function resolve(dir) {
  return path.join(__dirname, dir)
}

/**
 * 公共的抽离chunks
 */
const commonChunks = {
  libs: {
    name: 'chunk-libs',
    test: /[\\/]node_modules[\\/]/,
    priority: 2,
    chunks: 'initial'
  },
  lodashEs: {
    name: 'chunk-lodash-es',
    test: /[\\/]node_modules[\\/]lodash-es(.*)/,
    priority: 30
  },
  xss: {
    name: 'chunk-xss',
    test: /[\\/]node_modules[\\/]xss(.*)/,
    priority: 30
  },
  cssfilter: {
    name: 'chunk-xss',
    test: /[\\/]node_modules[\\/]cssfilter(.*)/,
    priority: 30
  },
  vueVirtualScrollList: {
    name: 'chunk-virtual-list',
    test: /[\\/]node_modules[\\/]vue-virtual-scroll-list(.*)/,
    priority: 30
  },
  sparkMd5: {
    name: 'chunk-spark-md5',
    test: /[\\/]node_modules[\\/]spark-md5(.*)/,
    priority: 30
  },
  emoji: {
    name: 'chunk-emoji',
    test: /[\\/]node_modules[\\/]emoji-mart-vue(.*)/,
    priority: 30
  },
  echarts: {
    name: 'chunk-echarts',
    test: /[\\/]node_modules[\\/]echarts(.*)/,
    priority: 30
  },
  zrender: {
    name: 'chunk-echarts',
    test: /[\\/]node_modules[\\/]zrender(.*)/,
    priority: 30
  },
  video: {
    name: 'chunk-video',
    test: /[\\/]node_modules[\\/]video(.*)/,
    priority: 30
  },
  wangeditor: {
    name: 'chunk-wangeditor',
    test: /[\\/]node_modules[\\/]wangeditor(.*)/,
    priority: 30
  },
  qrcode: {
    name: 'chunk-qrcode',
    test: /[\\/]node_modules[\\/]qrcode(.*)/,
    priority: 30
  },
  elementUI: {
    name: 'chunk-elementUI',
    priority: 30,
    test: /[\\/]node_modules[\\/]_?element-ui(.*)/
  },
  cheerio: {
    name: 'chunk-cheerio',
    priority: 30,
    test: /[\\/]node_modules[\\/]cheerio(.*)/
  },
  parse5: {
    name: 'chunk-cheerio',
    priority: 30,
    test: /[\\/]node_modules[\\/]parse5(.*)/
  },
  components: {
    name: 'chunk-components',
    test: resolve('src/components'),
    priority: 1,
    reuseExistingChunk: true
  }
}

function generateSplitChunksConfig(project) {
  const configMap = {
    novel: generateNovelChunks
  }
  // console.log(configMap[project])
  const chunks = configMap[project] && configMap[project]() || {}

  return {
    ...commonChunks,
    ...chunks
  }
}

/**
 * 生成编辑后台的代码分割规则
 * @returns
 */
function generateNovelChunks() {
  return {
    // commons: {
    //   name: 'chunk-commons-components',
    //   test: resolve('src/project/novel/components'),
    //   minChunks: 1,
    //   priority: 5,
    //   reuseExistingChunk: true
    // },
    // utils: {
    //   name: 'chunk-utils',
    //   test: resolve('src/project/novel/utils'),
    //   minChunks: 1,
    //   priority: 5,
    //   reuseExistingChunk: true
    // },
    // api: {
    //   name: 'chunk-api',
    //   test: resolve('src/project/novel/api'),
    //   minChunks: 1,
    //   priority: 21,
    //   reuseExistingChunk: true
    // },
    errorPage: {
      name: 'chunk-mix',
      test: resolve('src/project/novel/views/errorPage'),
      minChunks: 1,
      priority: 20,
      reuseExistingChunk: true
    },
    redirect: {
      name: 'chunk-mix',
      test: resolve('src/project/novel/views/redirect'),
      minChunks: 1,
      priority: 20,
      reuseExistingChunk: true
    },
    classifyManage: {
      name: 'chunk-classify',
      test: resolve('src/project/novel/views/classifyManage'),
      minChunks: 1,
      priority: 20,
      reuseExistingChunk: true
    },
    copyrightManage: {
      name: 'chunk-copyright',
      test: resolve('src/project/novel/views/copyrightManage'),
      minChunks: 1,
      priority: 20,
      reuseExistingChunk: true
    },
    novelManage: {
      name: 'chunk-novel',
      test: resolve('src/project/novel/views/novelManage'),
      minChunks: 1,
      priority: 20,
      reuseExistingChunk: true
    },
    pullLogs: {
      name: 'chunk-pullLogs',
      test: resolve('src/project/novel/views/pullLog'),
      minChunks: 1,
      priority: 20,
      reuseExistingChunk: true
    }
  }
}

module.exports = {
  generateSplitChunksConfig
}
