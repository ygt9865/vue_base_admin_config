const fs = require('fs')
const path = require('path')
const read = require('read')
const colors = require('colors')

/**
 * 版本号比较
 * @param {string} v1 第一个版本号
 * @param {string} v2 第二个版本号
 * @returns {-1 | 0 | 1} v1 大于 v2 返回 1，v2 小于 v2 返回-1，v1 等于 v2 返回 0
 */
function compareVersion(v1, v2) {
  if (!v1 && !v2) {
    return 0
  }

  if (!v1) {
    return -1
  }

  if (!v2) {
    return 1
  }

  const v1Stack = v1.split('.')
  const v2Stack = v2.split('.')
  const maxLen = Math.max(v1Stack.length, v2Stack.length)

  for (let i = 0; i < maxLen; i++) {
    // 必须转整，否则按照字符顺序比较大小
    const prevVal = ~~v1Stack[i]
    const currVal = ~~v2Stack[i]

    if (prevVal > currVal) {
      return 1
    }

    if (prevVal < currVal) {
      return -1
    }
  }
  return 0
}

/**
 * 校验版本号格式
 * @param {string} version 版本号
 * @returns {boolean} 是否通过
 */
function isVersion(version) {
  // 限制为 x.x.x
  return /([0-9]+\.{1}){2}[0-9]+/.test(version)
}

/**
 * package.json路径
 */
const packagePath = path.resolve(__dirname, './../package.json')

/**
 * 获取package.json内容
 * @returns {Object} package.json内容
 */
function getPackageJson() {
  const content = fs.readFileSync(packagePath)
  return JSON.parse(content)
}

/**
 * 获取当前的版本号
 * @returns {string} 版本号
 */
function getCurrentVersion() {
  const content = getPackageJson()
  // console.log(content.version)
  return content.version
}

/**
 * 更新版本号
 * @param {string} version 新的版本号
 * @returns {void}
 */
function updateVersion(version) {
  try {
    const content = getPackageJson()
    content.version = version
    fs.writeFileSync(packagePath, JSON.stringify(content, null, 2))
    console.log(colors.green(`version已成功修改为: ${version}`))
  } catch (e) {
    throw new Error(e.message)
  }
}

/**
 * 获取用户输入
 * @param {string} options.prompt 提示信息
 * @returns {Promise} promise实例
 */
function readInput({ prompt }) {
  return new Promise((resolve, reject) => {
    read({ prompt }, (err, res) => {
      if (err) return reject(err)
      resolve(res)
    })
  })
}

// eslint-disable-next-line require-jsdoc
async function getUserVersion() {
  try {
    const version = await readInput({ prompt: '请输入新的版本号：' })
    if (!isVersion(version)) {
      throw new Error('版本号格式不符合 x.x.x -> ' + colors.red(version))
    }
    return version
  } catch (e) {
    throw new Error(e.message)
  }
}

const currentVersion = getCurrentVersion()

// eslint-disable-next-line require-jsdoc
async function main() {
  try {
    const res = await readInput({ prompt: `当前版本号为：${colors.red(currentVersion)}，是否修改版本号 ？ y(yes) or n(no)` })
    if (['yes', 'y'].includes(res.toLowerCase())) {
      const version = await getUserVersion()
      // 校验版本号大小
      const flag = compareVersion(version, currentVersion)
      if (flag === 1) {
        // 版本通过
        updateVersion(version)
      } else {
        // 版本不通过
        console.log(`${colors.red('新的版本号要大于旧的版本号(' + currentVersion + ')')}`)
      }
    } else {
      console.log(`${colors.yellow('取消修改版本号')}`)
    }
  } catch (e) {
    console.log(colors.red(e))
    // throw new Error('发生错误', e)
  }
}

main()
