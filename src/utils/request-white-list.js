export default [
  { // 登录
    method: 'post',
    url: '/marketing/oe/auth/login'
  },
  { // 获取用户的信息
    method: 'get',
    url: '/marketing/oe/auth/self/info'
  },
  { // 修改个人资料信息
    method: 'put',
    url: '/marketing/oe/auth/self/info'
  },
  { // 获取当前登录用户的权限
    method: 'get',
    url: '/marketing/oe/auth/self/permission'
  }
]
