const title = '笔墨文学编辑后台'

/**
 * 设置页面标题
 * @param {string} pageTitle 页面标题
 * @returns {string} 页面标题
 */
export default function getPageTitle(pageTitle) {
  if (pageTitle) {
    return `${pageTitle} - ${title}`
  }
  return `${title}`
}
