// 路由白名单，存放路由的别名
const whiteList = ['RoleManage', 'AccountList', 'RoleList']

/**
 * 过滤白名单中的路由
 * @param {any[]} routerList 路由列表
 * @returns {any[]} 过滤后的路由列表
 */
export default routerList => routerList.filter(item => !whiteList.includes(item.menuAlias))
