import Vue from 'vue'
import {
  Scrollbar,
  Button,
  Pagination,
  Loading,
  MessageBox,
  Message,
  Popover,
  Tabs,
  TabPane,
  Form,
  FormItem,
  Cascader,
  CascaderPanel,
  Input,
  Select,
  Option,
  Checkbox,
  Switch,
  Table,
  TableColumn,
  Dialog,
  Tree
} from 'element-ui'

import { isPlainObject } from 'lodash-es'

Vue.prototype.$ELEMENT = { size: 'small', zIndex: 3000 }

Vue.use(Button)
Vue.use(Pagination)
Vue.use(Popover)
Vue.use(Tabs)
Vue.use(TabPane)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Input)
Vue.use(Checkbox)
Vue.use(Switch)
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(Dialog)
Vue.use(Select)
Vue.use(Option)
Vue.use(Tree)
Vue.use(Scrollbar)
Vue.use(Cascader)
Vue.use(CascaderPanel)

// 挂载 Loading
Vue.prototype.$loading = Loading

// 挂载MessageBox实例方法
Vue.prototype.$msgbox = MessageBox
Vue.prototype.$alert = MessageBox.alert
Vue.prototype.$confirm = (message = '', title = '', options = {}) => {
  // title 传递为options对象处理
  if (title && typeof title !== 'string') {
    options = title
    title = ''
  }

  const defaultOptions = {
    type: 'warning',
    customClass: 'message-box-warning',
    center: true,
    showClose: false,
    confirmButtonClass: 'btn-confirm',
    cancelButtonClass: 'btn-cancel'
  }
  return MessageBox.confirm(message, title, Object.assign(defaultOptions, options))
}
Vue.prototype.$prompt = MessageBox.prompt

// Message
// // 偏移量
// let messageOffset = getWindowInnerHeight() / 2
// // 注册窗口变化重新设定
// document.addEventListener('resize', () => {
//   messageOffset = getWindowInnerHeight() / 2
// })
Vue.prototype.$message = (options = {}) => {
  // 传入提示文本处理，统一为对象
  let messageOptions = {}
  if (!isPlainObject(options)) {
    messageOptions.message = options
  } else {
    messageOptions = options
  }

  const defaultOptions = {
    // offset: messageOffset
  }
  return Message(Object.assign(defaultOptions, messageOptions))
}
/**
 * 创建不同状态的message实例
 * @param {'success' | 'info' | 'error' | 'warning'} type 状态值
 * @returns {Message} 不同状态的Message
 */
function createMessage(type = 'info') {
  return (options) => {
    // 传入提示文本处理，统一为对象
    let messageOptions = {}
    if (!isPlainObject(options)) {
      messageOptions.message = options
    } else {
      messageOptions = options
    }

    const defaultOptions = {
      // offset: messageOffset,
      customClass: `message-${type}`,
      type
    }
    return Message(Object.assign(defaultOptions, messageOptions))
  }
}
const MESSAGE_TYPES = ['success', 'info', 'error', 'warning']
MESSAGE_TYPES.forEach(type => {
  Vue.prototype.$message[type] = createMessage(type)
})
