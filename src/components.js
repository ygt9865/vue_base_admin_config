import Vue from 'vue'

const files = require.context('./components', true, /\.js$/)

const components = files.keys().reduce((modules, modulePath) => {
  const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1').replace('/index', '')
  const value = files(modulePath)
  modules[moduleName] = value.default
  return modules
}, {})

Object.keys(components).forEach(key => {
  Vue.use(components[key])
})
