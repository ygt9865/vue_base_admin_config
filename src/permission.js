import router from './router'
import store from './store'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import { isRedirectRouter, hasRouterPermission, setRoutesIndex } from '@engineer/utils'
import { getToken } from '@utils/auth'
import getPageTitle from '@utils/get-page-title'

NProgress.configure({ showSpinner: false })

// 路由白名单
const whiteList = ['/login']
let isPermissionBeNull = false // 标记当前登录用户的路由权限是否为空

document.title = '笔墨文学快应用平台'

router.beforeEach(async(to, from, next) => {
  NProgress.start()
  // 判断当前的路由的query中是否携带了临时token
  const { account_token: accountToken = '' } = to.query

  // 判断缓存中的token是否为空
  if (!store.getters.token) {
    store.dispatch('user/setToken')
  }

  try {
    if (accountToken) {
      // 设置session缓存中从sso过来的信息
      sessionStorage.setItem('sso', 1)
      await store.dispatch('user/accountTokenLogin', { accountToken })
      // 重置携带的account_token
      delete to.query.account_token
    }

    if (getToken()) {
      if (to.path === '/login') {
        // 区分当前是否从统一入口登录
        next({ path: '/' })
        NProgress.done()
      } else {
        const hasRoles = store.getters.roles && store.getters.roles.length
        // 判断是否已经获取了用户的权限列表
        if (hasRoles) {
          // 判断当前用户的动态路由是否为空
          if (store.getters.addRouters.length === 1 && !isPermissionBeNull) {
            isPermissionBeNull = true
            next({ path: '/401', replace: true, query: { noGoBack: true }})
            return
          }

          // 判断当前用户即将要去的路由是否在允许的路由中
          if (isRedirectRouter(to) || hasRouterPermission(store.getters.permission_routers, to)) {
            next()
          } else {
            next({ path: '/401', replace: false, query: { noGoBack: false }})
          }
        } else {
          // 获取用户的信息
          const userInfo = await store.dispatch('user/getUserInfo')
          const { name } = userInfo
          // 设置包名 + 名称
          document.title = getPageTitle(name)
          // 获取用户的权限
          const { permissionTree } = await store.dispatch('user/getUserPermission')
          // 根据用户的树形权限数据生成动态的路由菜单
          const accessRoutes = await store.dispatch('permission/generateRoutes', { permissionTree })
          // 路由菜单首页的处理，如果该用户没有访问全局首页的权限，则选择第一个目录页面作为首页
          setRoutesIndex(accessRoutes, { affix: true })
          // 401 page must be placed at the end !!!
          accessRoutes.push({ path: '*', redirect: '/401', hidden: true })
          // 动态添加可访问路由
          router.addRoutes(accessRoutes)

          // hack方法，以确保addRoutes是完整的
          // 设置replace: true，这样导航将不会留下历史记录
          next({ ...to, replace: true })
        }
      }
    } else {
      if (whiteList.indexOf(to.path) !== -1) {
        next()
      } else {
        // 判断是否有重定向回sso的操作，解决刷新页面token失效跳转的问题
        const redirect = sessionStorage.getItem('redirect')
        console.log(redirect)
        if (redirect) {
          sessionStorage.removeItem('redirect')
          store.dispatch('user/logOut')
        } else {
          next(`/login`)
        }
      }
    }
  } catch (e) {
    // 发生错误，退出登录并删除token
    store.dispatch('user/logOut')
  } finally {
    NProgress.done()
  }
})

router.afterEach(() => {
  NProgress.done()
})
