import path from 'path'
import { setRouterRedirectRoute } from '@engineer/utils'
import { constantRouterMap } from '@/router'
import routerComponentMap from '@/router/routerComponentMap'

/**
 * 取消缓存的路由列表
 */
const noCacheRouteList = []

/**
 * 生成路由菜单树形结构
 * @param {array} treeData 树形结构数据
 * @param {string} parentPath 父级路由的路径
 * @returns {array} 路由菜单
 */
const generateRouteMenuTree = (treeData, parentPath = '') => {
  const res = []
  treeData.forEach(item => {
    if (item.type === 3) { // 排除按钮级的权限数据
      return
    }
    const temp = {
      path: path.resolve(parentPath, item.clientRoute), // 路由
      component: routerComponentMap[item.menuAlias] ? routerComponentMap[item.menuAlias] : routerComponentMap['NonentityRouter'], // 从路由组件哈希表中拿
      redirect: item.redirectRoute, // 路由重定向
      name: item.menuAlias, // 菜单名
      hidden: !item.isShow, // 是否显示
      meta: {
        title: item.menuName, // 菜单标题
        icon: item.icon // 图标
      },
      children: []
    }

    // 判断路由是否需要取消缓存
    if (noCacheRouteList.includes(item.menuAlias)) {
      temp.meta.noCache = true
    }

    // 判断子级路由
    if (item.children && item.children.length) {
      temp.children = generateRouteMenuTree(item.children, temp.path)
      // if (!temp.children.length) { // 不存在子级路由，删除该属性
      //   delete temp.children
      // }
    }

    res.push(temp)
  })
  return res
}

/**
 * 生成权限节点树形结构
 * @param {array} permissionTree 树形结构数据
 * @param {string} parentPath 父级路由的路径
 * @returns {array} 路由菜单
 */
function generateRoleTreeData({ permissionTree, parentPath = '', apiPermission = [] } = {}) {
  const res = []
  // console.log(apiPermission)
  permissionTree.forEach(item => {
    const temp = {
      menuid: item.menuid,
      type: item.type,
      path: path.resolve(parentPath, item.clientRoute), // 路由
      name: item.menuAlias // 菜单名
    }

    // 按钮级的权限数据
    if (item.type === 3) {
      temp.roles = []
      delete item.path
      apiPermission.forEach(item => {
        const { nodeid = 0 } = item
        if (temp.menuid === nodeid) {
          temp.roles.push(item)
        }
      })
    }

    // 判断子级路由
    if (item.children && item.children.length) {
      temp.children = generateRoleTreeData({ permissionTree: item.children, parentPath: temp.path, apiPermission })
    }

    res.push(temp)
  })
  return res
}

const state = {
  routers: constantRouterMap, // 包含静态和动态的路由
  addRouters: [], // 动态路由
  roleTree: [] // 权限节点
}

const mutations = {
  /**
   * 存储路由信息
   * @param {Object} state state
   * @param {array} routers 路由
   * @returns {void}
   */
  SET_ROUTERS: (state, routers) => {
    state.addRouters = routers
    state.routers = constantRouterMap.concat(routers)
  },

  /**
   * 存储权限节点
   * @param {Object} state state
   * @param {array} roleTree 权限节点
   * @returns {void}
   */
  SET_ROLE_TREE: (state, roleTree) => {
    state.roleTree = roleTree
  }
}

const actions = {
  generateRoutes({ commit }, { permissionTree, apiPermission }) {
    return new Promise(resolve => {
      console.log('树形数据', permissionTree)
      const roleTree = generateRoleTreeData({ permissionTree, apiPermission })
      let accessedRouters = generateRouteMenuTree(permissionTree)
      accessedRouters = setRouterRedirectRoute(accessedRouters)
      console.log('动态路由', accessedRouters)
      console.log('roleTree', roleTree)
      commit('SET_ROUTERS', accessedRouters)
      commit('SET_ROLE_TREE', roleTree)
      resolve(accessedRouters)
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
