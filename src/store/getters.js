const getters = {
  sidebar: state => state.app.sidebar,
  size: state => state.app.size,
  device: state => state.app.device,
  visitedViews: state => state.tagsView.visitedViews,
  cachedViews: state => state.tagsView.cachedViews,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  userId: state => state.user.userId,
  roleId: state => state.user.roleId,
  roles: state => state.user.roles,
  apiPermission: state => state.user.apiPermission, // api权限
  allocationRoleTree: state => state.user.allocationRoleTree,
  allocationRoleList: state => state.user.allocationRoleList,
  userPermissionTree: state => state.user.userPermissionTree,
  roleTree: state => state.permission.roleTree,
  permission_routes: state => state.permission.routers,
  addRouters: state => state.permission.addRouters,
  errorLogs: state => state.errorLog.logs
}
export default getters
