import Vue from 'vue'
import { SvgIcon } from '@engineer/vue-components'// svg component

// register globally
// Vue.component('svg-icon', SvgIcon)
Vue.use(SvgIcon)

const req = require.context('./svg', false, /\.svg$/)
const requireAll = requireContext => requireContext.keys().map(requireContext)
requireAll(req)
