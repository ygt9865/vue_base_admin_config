import Layout from '@/layout'

export default {
  SystemSetup: Layout, // 系统设置
  // RoleList: () => import('@agents/views/systemSetup/roleList'), // 角色列表
  // AccountList: () => import('@agents/views/systemSetup/accountList'), // 账号列表
  // UserInfo: () => import('@agents/views/systemSetup/userInfo'), // 个人资料
  NonentityRouter: () => import('@/views/errorPage/404') // 匹配不到任何的路由组件，使用这个页面组件
}
