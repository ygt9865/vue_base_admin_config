import Vue from 'vue'
import App from '@/App'
import store from '@/store'
import router from '@/router'

import 'normalize.css/normalize.css' // a modern alternative to CSS resets

import BackendPermission from '@engineer/backend-permission'

import '@/styles/element-variables.scss'

import '@/styles/index.scss' // global css

import '@/icons' // icon
import '@/permission' // permission control
import '@/utils/error-log' // error log

// 按需引入element-ui
import '@/plugin/element-ui'

// 全局注册component组件
import './components'

// import * as filters from '@/filters' // global filters
// register global utility filters
// Object.keys(filters).forEach(key => {
//   Vue.filter(key, filters[key])
// })

Vue.use(BackendPermission, { store })

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
