import request from '@utils/request'

/**
 * 根据指定的角色ID获取该角色下的权限列表
 * @param {number} roleid 角色ID
 * @returns {Promise} promise请求实例
 */
export const apiGetRolePermission = roleid => request.get(`/agent/permission/role/${roleid}/node`)

/**
 * 编辑指定角色ID的权限
 * @param {number} roleid 角色ID
 * @param {array} permissionids 角色权限，用英文逗号","隔开。如：1,2,3,4,5
 * @returns {Promise} promise请求实例
 */
export const apiEditRolePermission = ({ roleid, permissionids }) => request.patch(`/agent/permission/role/${roleid}/node`, { permissionids })

/**
 * 编辑指定角色ID的信息
 * @param {number} roleid 角色ID
 * @param {number} parentRoleid 父级角色ID
 * @param {string} name 角色名
 * @param {string} remark 备注
 * @returns {Promise} promise请求实例
 */
export const apiEditRoleInfo = ({ roleid, parentRoleid, name, remark }) => request.patch(`/agent/permission/role/${roleid}`, { parentRoleid, name, remark })

/**
 * 获取角色列表
 * @returns {Promise} promise请求实例
 */
export const apiGetRoleList = () => request.get('/agent/permission/role')

/**
 * 添加角色信息
 * @param {string} name 角色名
 * @param {number} parentRoleid 父级角色ID
 * @param {string} permissionids 权限列表集合，用英文逗号隔开
 * @param {string} remark 备注信息
 * @returns {Promise} promise请求实例
 */
export const apiAddRole = ({ name, parentRoleid, permissionids, remark }) => request.post('/agent/permission/role', { name, parentRoleid, permissionids, remark })

/**
 * 编辑角色信息
 * @param {number} roleid 角色ID
 * @param {string} name 角色名
 * @param {number} parentRoleid 父级角色ID
 * @param {string} remark 备注信息
 * @returns {Promise} promise请求实例
 */
export const apiEditRole = ({ roleid, name, parentRoleid, remark }) => request.patch(`/agent/permission/role/${roleid}`, { name, parentRoleid, remark })

/**
 * 获取当前用户可分配的角色列表
 * @returns {Promise} promise请求实例
 */
export const apiGetAllocationRoleList = () => request.get('/agent/admin/self/roles')

/**
 * 获取可操作的角色列表(用于添加/编辑角色)，返回当前用户所属角色的下级（包括当前层级）
 * @returns {Promise} promise请求实例
 */
export const apiGetOperableRoleList = () => request.get('/agent/permission/operable/role')
