// import request from '@/utils/request'
import request from '@utils/request'

/**
 * 用户登录
 * @param {Object} data 请求数据
 * @returns {Promise} promise请求实例
 */
export const apiLogin = data => request.post('/agent/auth/login', data)

/**
 * accountToken换算真正的token
 * @param {string} accountToken 临时登陆token
 * @returns {Promise} promise请求实例
 */
export const apiAccountTokenLogin = ({ accountToken }) => request.post('/agent/auth/login', { tmpToken: accountToken })

/**
 * 获取当前登录用户的信息
 * @returns {Promise} promise请求实例
 */
export const apiGetUserInfo = () => request.get('/agent/info')

/**
 * 修改个人资料信息
 * @param {string} email 邮箱账户（非必填）
 * @param {string} password 密码（非必填）
 * @returns {Promise} promise请求实例
 */
export const apiEditUserInfo = ({ email, password }) => request.put('/agent/info', { email, password })

/**
 * 获取当前登录用户的权限
 * @returns {Promise} promise请求实例
 */
export const apiGetUserPermission = () => request.get('/agent/admin/self/permission')
