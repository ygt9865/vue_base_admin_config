# vue_admin_base

### 本地开发
由于项目部分npm包是采用私有npm包，需要在本地 `npm` 源里面配置私有npm的地址。
#### npm私有地址
使用 nrm 进行npm源管理
```
# nrm安装
npm install nrm -g
# 或
yarn add nrm -g

# nrm 查看源
nrm ls

# nrm 注册私有npm源
# registry 源名
# url 源地址
nrm add <registry> <url>
# 例： nrm add engineer http://8.129.12.137:5678

# nrm 切换源
nrm use <源名>

# nrm 删除源
nrm del <源名>
```

```
# 切换npm源到私有源上，再进行安装
nrm use engineer

# 安装依赖
npm install
# 或
# 推荐使用yarn
yarn install

# 本地启动
npm run dev
# 或
yarn dev
```

### 开发规范
#### 文件存放位置
1、视图vue文件存放在 `views` 中 <br>
2、与整体布局有关的存放在 `layout` 中 <br>
3、定义api接口的js文件存放在 `api` 中 <br>
4、svg格式文件存放在 `icons/svg` 中，webpack会将这个目录下的svg文件进行处理，配合 `SvgIcon` 组件使用 <br>
5、第三方组件相关的js文件存放在 `plugin` 中，比如 `element-ui` 的按需加载 <br>
6、由于是动态路由，所以 `router` 中并不存放路由定义，有新增的路由，需要更新路由映射表 `routerComponentsMap.js` 和 `md` 定义路由文件 <br>
7、全局样式相关的，一律存放在 `styles` 中，根据新增的内容，在对应的文件里面写对应的代码 <br>
8、工具类函数相关的以及涉及到业务相关的纯函数存放在 `utils` 中，业务相关的纯函数可以一个函数一个文件或者按照业务进行区分 <br>

#### 文件命名
1、文件以及目录均采用 `小写带横线分割` 的方式，组件命名例外，不允许采用 `大驼峰` 或 `小驼峰` 等方式，url不区分大小写，也是按照vue官方推荐的文件命名规范。 <br>
2、文件名包含多个单词时，单词之间建议使用半角的连词线 ( - ) 分隔。 <br>
3、组件命名采用 `大驼峰` 的命名，和组件的 `name` 保持一致，禁止两者不一样。 <br>
4、`views` 中的按照模块进行分组命名，从二级路由开始，避免过分嵌套文件层级。 <br>
举例：账户中心 -> 账户管理 -> 我方投放账户 <br>
views -> account-manage -> our-account <br>
忽略一级，从二级开始考虑，按照模块命名，用 `-` 中横线分割，`views` 下最多有两级不包括（components目录）<br>

#### 组件规范
##### 全局组件：多个地方使用到的组件
可分为 功能组件（表格）和业务组件（添加/编辑代理商表单） <br><br>

功能组件参考 `AdTable` 的写法 <br>
业务组件参考 `CustomerForm` 的写法，需要支持作为指令调用 <br>
##### 页面组件
放在所属模块的 `components` 目录内。<br>
例如：

```
-| developer-account
  -| components
    -| DeveloperAccountForm.vue
```

#### 函数命名规范
##### API
1、api相关的均以 `api` 开头
2、api功能区分 `api` 紧跟的单词<br>

 <br>
 - remove/delete 删除
 - update 更新
 - create 添加
 - get 查询
 <br>

3、查询接口支持列表查询参数以 `list` 结尾

##### 其他函数
命名方法：小驼峰式命名法 <br>
命名规范：前缀应当为动词 <br>
命名建议：可使用常见动词约定 <br>

| 动词 | 含义 | 返回值 |
| --- | --- | ---- |
| can | 判断是否可执行某个动作(权限) | boolean，是否可执行 |
| has | 判断是否含有某个值 | boolean，是否含有 |
| is | 判断是否为某个值 | boolean，是否为某个值 |
| get | 获取某个值 | 函数返回一个非布尔值 |
| set | 设置某个值 | 无返回值 |
| load | 加载数据 | 无返回值 |



### 发布流程
分为三步。
#### 第一步，检查 package.json 的 version
```
npm run build:version
# 或
yarn build:version
```
会执行脚本命令，读取version值，并进行修改，需要严格按照版本发布的规则去书写版本号

#### 第二步，检查 分支合并情况以及私有依赖包版本是否符合一致
私有依赖包需要直接锁定版本，不能采用 `^` `~` 等符号。

#### 第三步，构建线上版本，并进行对应的打 tag 流程
```
npm run build:prod
# 或
yarn build:prod
```


### 项目引用的私有依赖包

#### @engineer/utils
优先采用 私有包 @engineer/utils 里面的方法，其次是 lodash-es 里面的方法，当两个包的函数无法解决当前业务需求，需要额外增加时，需要考虑几点：<br>
1、函数是否通用，属于工具类函数 <br>
2、是否属于通用业务逻辑，不属于该系统特定的逻辑 <br>
当以上点符合时，可以考虑放在 私有包 @engineer/utils 里面。 <br>
不符合时，放在 该项目的 utils 里面，并注明函数的注释，需要严格遵守 jsdoc 规范编写函数。

#### @engineer/vue-components
采用按需加载的方式

#### @engineer/backend-permission

#### @engineer/input-number

#### @engineer/scroll-load


### 项目引用第三方库

#### lodash-es

#### axios
基于 `axios` 拓展的功能：<br>
1、接口无权限拦截 <br>
2、接口取消请求 <br>
3、对 post、put、patch请求做全局的字符串型类型数据前后空格过滤，支持在api接口定义时过滤指定字段不做处理 <br>

#### element-ui
按需加载，有新增的需要在 项目根目录 `src/plugin/element-ui.js` 里面增加组件引入 <br>
`注：不允许在页面组件内部引入单个组件的方式，一律放在上面说的文件里面引入，为了方便抽离成为单独的 element-ui chunks`
